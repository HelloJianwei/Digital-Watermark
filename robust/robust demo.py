import cv2
import numpy
from PIL import Image
from sys import exit

demo_mode=1
'''演示模式-0:普通 1:攻击'''


if demo_mode==0: 
    oriImg=cv2.imread("F:/Projects/digital watermark/demo/robust/normal/lake.jpg")
    markImg=oriImg

    sourceFile='F:/Projects/digital watermark/demo/robust/normal/love.jpg'
    watermark=Image.open(sourceFile)
    if watermark.mode != '1':
        if input('警告：仅支持黑白水印，水印将被转为黑白，是否继续？(y/n)?') != 'y':
            exit(0)
        watermark = watermark.convert('1')
        
        
    for m in range(0,watermark.size[0]):
            for n in range(0,watermark.size[1]):
                x=m*8;
                y=n*8;
                
                subPic=oriImg[x:x+8,y:y+8,2]      
                dct_subPic=cv2.dct(numpy.float32(subPic))
            
                meanValue=(dct_subPic[1,5]+dct_subPic[2,4]+dct_subPic[0,6]+dct_subPic[5,1]+dct_subPic[6,0]+dct_subPic[4,2])/6
                
                if watermark.getpixel((m,n))==0:
                    dct_subPic[3,3]=meanValue-23
                else:
                    dct_subPic[3,3]=meanValue+23
                #dct_subPic[3,3]=(1+a*0.1)*dct_subPic[3,3]
                
                mark_subPic=cv2.idct(dct_subPic)
                markImg[x:x+8,y:y+8,2]=mark_subPic[0:8,0:8]
                
    cv2.imwrite('F:/Projects/digital watermark/demo/robust/normal/mark.png',markImg)            
                
                
                
                
      #提取          
    markImg=cv2.imread("F:/Projects/digital watermark/demo/robust/normal/mark.png")
    extract=Image.new('1', watermark.size)

    for m in range(0,min(extract.size[0],int(markImg.shape[1]/8))):
            for n in range(0,min(extract.size[1],int(markImg.shape[0]/8))):
                x=m*8;
                y=n*8;
                
                submarkPic=markImg[x:x+8,y:y+8,2]
                dct_submarkPic=cv2.dct(numpy.float32(submarkPic))
                
                
                meanValue=(dct_submarkPic[1,5]+dct_submarkPic[2,4]+dct_submarkPic[0,6]+dct_submarkPic[5,1]+dct_submarkPic[6,0]+dct_submarkPic[4,2])/6
                
                if dct_submarkPic[3,3]-meanValue<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)
                    
                '''if dct_submarkPic[3,3]/float(dct_subPic[3,3])-1<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)'''

    extract.save('F:/Projects/digital watermark/demo/robust/normal/ext.png')
    print('成功！')
    
    
    
else:
    markImg=cv2.imread("F:/Projects/digital watermark/demo/robust/attack/mark-resize.png")
    extract=Image.new('1', (250,203))

    for m in range(0,min(extract.size[0],int(markImg.shape[0]/8))):
            for n in range(0,min(extract.size[1],int(markImg.shape[1]/8))):
                x=m*8;
                y=n*8;
                
                submarkPic=markImg[x:x+8,y:y+8,2]
                dct_submarkPic=cv2.dct(numpy.float32(submarkPic))
                
                
                meanValue=(dct_submarkPic[1,5]+dct_submarkPic[2,4]+dct_submarkPic[0,6]+dct_submarkPic[5,1]+dct_submarkPic[6,0]+dct_submarkPic[4,2])/6
                
                if dct_submarkPic[3,3]-meanValue<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)
                    
                '''if dct_submarkPic[3,3]/float(dct_subPic[3,3])-1<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)'''

    extract.save('F:/Projects/digital watermark/demo/robust/attack/ext-resize.png')  

    markImg=cv2.imread("F:/Projects/digital watermark/demo/robust/attack/mark-brush.png")
    extract=Image.new('1', (250,203))

    for m in range(0,min(extract.size[0],int(markImg.shape[0]/8))):
            for n in range(0,min(extract.size[1],int(markImg.shape[1]/8))):
                x=m*8;
                y=n*8;
                
                submarkPic=markImg[x:x+8,y:y+8,2]
                dct_submarkPic=cv2.dct(numpy.float32(submarkPic))
                
                
                meanValue=(dct_submarkPic[1,5]+dct_submarkPic[2,4]+dct_submarkPic[0,6]+dct_submarkPic[5,1]+dct_submarkPic[6,0]+dct_submarkPic[4,2])/6
                
                if dct_submarkPic[3,3]-meanValue<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)
                    
                '''if dct_submarkPic[3,3]/float(dct_subPic[3,3])-1<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)'''

    extract.save('F:/Projects/digital watermark/demo/robust/attack/ext-brush.png') 

    markImg=cv2.imread("F:/Projects/digital watermark/demo/robust/attack/mark-compress.jpg")
    extract=Image.new('1', (250,203))

    for m in range(0,min(extract.size[0],int((markImg.shape[0]-8)/8))):
            for n in range(0,min(extract.size[1],int((markImg.shape[1]-8)/8))):
                x=m*8;
                y=n*8;
                
                submarkPic=markImg[x:x+8,y:y+8,2]
                dct_submarkPic=cv2.dct(numpy.float32(submarkPic))
                
                
                meanValue=(dct_submarkPic[1,5]+dct_submarkPic[2,4]+dct_submarkPic[0,6]+dct_submarkPic[5,1]+dct_submarkPic[6,0]+dct_submarkPic[4,2])/6
                
                if dct_submarkPic[3,3]-meanValue<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)
                    
                '''if dct_submarkPic[3,3]/float(dct_subPic[3,3])-1<0:
                    extract.putpixel((m,n), 0)
                else:
                    extract.putpixel((m,n), 255)'''

    extract.save('F:/Projects/digital watermark/demo/robust/attack/ext-compress.png') 
    print('成功！')    