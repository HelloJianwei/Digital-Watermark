# -*- coding: utf-8 -*-
from PIL import Image

demo_mode=1
'''演示模式-0:普通 1:攻击'''

if demo_mode==0: 
    oriImg=Image.open("F:/Projects/digital watermark/demo/weak/normal/lake.jpg")
    markImg=oriImg

    sourceFile='F:/Projects/digital watermark/demo/weak/normal/love.jpg'
    watermark=Image.open(sourceFile);
    if watermark.mode != '1':
        if input('警告：仅支持黑白水印，水印将被转为黑白，是否继续？(y/n)?') == 'y':
            watermark = watermark.convert('1')
        
    if markImg.mode != 'RGB':
        markImg = markImg.convert('RGB')
    if watermark.size != markImg.size:
        watermark = watermark.resize(markImg.size)
    for x in range(markImg.size[0]):
        for y in range(markImg.size[1]):
            pixel = markImg.getpixel((x, y))
            info = watermark.getpixel((x, y))
            markImg.putpixel((x, y), ((pixel[0] & ((1 << 8) - 2)) | (1 if info == 255 else 0), pixel[1], pixel[2]))
    mark=Image.new('1',markImg.size)
    mark=markImg
    mark.save('F:/Projects/digital watermark/demo/weak/normal/mark.png')

    markImg=Image.open("F:/Projects/digital watermark/demo/weak/normal/mark.png")
    extract=Image.new('1', watermark.size)
    for x in range(markImg.size[0]):
        for y in range(markImg.size[1]):
            pixel = markImg.getpixel((x, y))
            extract.putpixel((x, y), 255 if (pixel[0] & 1) == 1 else 0)
    extract.save('F:/Projects/digital watermark/demo/weak/normal/ext.png')
    print('成功！')
    
    
    
else:
    markImg=Image.open("F:/Projects/digital watermark/demo/weak/attack/mark-resize.png")
    extract=Image.new('1', markImg.size)
    for x in range(markImg.size[0]):
        for y in range(markImg.size[1]):
            pixel = markImg.getpixel((x, y))
            extract.putpixel((x, y), 255 if (pixel[0] & 1) == 1 else 0)
    extract.save('F:/Projects/digital watermark/demo/weak/attack/ext-resize.png')
    
    markImg=Image.open("F:/Projects/digital watermark/demo/weak/attack/mark-brush.png")
    extract=Image.new('1', markImg.size)
    for x in range(markImg.size[0]):
        for y in range(markImg.size[1]):
            pixel = markImg.getpixel((x, y))
            extract.putpixel((x, y), 255 if (pixel[0] & 1) == 1 else 0)
    extract.save('F:/Projects/digital watermark/demo/weak/attack/ext-brush.png')
    
    markImg=Image.open("F:/Projects/digital watermark/demo/weak/attack/mark-compress.jpg")
    extract=Image.new('1', markImg.size)
    for x in range(markImg.size[0]):
        for y in range(markImg.size[1]):
            pixel = markImg.getpixel((x, y))
            extract.putpixel((x, y), 255 if (pixel[0] & 1) == 1 else 0)
    extract.save('F:/Projects/digital watermark/demo/weak/attack/ext-compress.png')
    print('成功！')